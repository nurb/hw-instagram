<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Post
 *
 * @ORM\Table(name="post")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PostRepository")
 * @Vich\Uploadable
 */
class Post
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="posts")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     * @ORM\Column(name="image", type="string")
     */
    private $image;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="post_file", fileNameProperty="image")
     *
     * @Assert\File(
     *     mimeTypes = { "image/gif", "image/jpeg", "image/png" },
     *     mimeTypesMessage = "Недопустимый тип данных ({{ type }}). Допустимы: {{ types }}.",
     *     maxSize = "2000k",
     *     maxSizeMessage = "Размер файла привышает 2 мегабайта"
     * )
     * @Assert\Image(
     *     minRatio = "0.55",
     *     maxRatio = "1.82",
     *     minHeight = "300",
     *     minWidth = "300"
     * )
     * @var File
     */
    private $imageFile;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User", inversedBy="likedPosts", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="post_likes")
     */
    private $likers;

    private $isLiked;

    private $likersCount;


    public function __construct()
    {
        $this->likers = new ArrayCollection();
    }



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Post
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Post
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function __toString()
    {
        return $this->caption ?: '';
    }


    /**
     * @param mixed $user
     * @return Post
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return mixed
     */
    public function getLikers(): ArrayCollection
    {
        return $this->likers;
    }

    public function addLikers(User $user)
    {
        $this->likers->add($user);
    }

    public function removeLikers(User $user)
    {
        $this->likers->removeElement($user);
    }

    public function hasLikers(User $user)
    {
        return $this->likers->contains($user);
    }

    /**
     * @return mixed
     */
    public function getisLiked($user)
    {
        return $this->likers->contains($user);
    }

    /**
     * @return mixed
     */
    public function getLikersCount()
    {
        return count($this->likers);
    }

}

