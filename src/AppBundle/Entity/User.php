<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Post", mappedBy="likers", cascade={"persist", "remove"})
     */
    private $likedPosts;


    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="User", mappedBy="follows")
     */
    private $followers;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="User", inversedBy="followers")
     * @ORM\JoinTable(name="followers")
     */
    private $follows;

    private $amIFollower;


    /**
     * @var mixed
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Post", mappedBy="user")
     */
    private $posts;

    public function __construct()
    {
        $this->posts = new ArrayCollection();
        $this->likedPosts = new ArrayCollection();
        parent::__construct();
    }

    /**
     * @param mixed $posts
     * @return User
     */
    public function setPosts($posts)
    {
        $this->posts = $posts;
        return $this;
    }

    public function __toString()
    {
        return $this->email ?: '';
    }

    /**
     * @return mixed
     */
    public function getFollows(): array
    {
        return $this->follows;
    }

    public function addFollows(User $user)
    {
        $this->follows->add($user);
    }

    public function removeFollows(User $user)
    {
        $this->follows->removeElement($user);
    }

    public function hasFollows(User $user)
    {
        return $this->follows->contains($user);
    }

    /**
     * @return mixed
     */
    public function getFollowers()
    {
        return $this->followers;
    }

    public function addFollowers(User $user)
    {
        $this->followers->add($user);
    }

    public function hasFollowers(User $user)
    {
        return $this->followers->contains($user);
    }

    /**
     * @return ArrayCollection
     */
    public function getLikedPosts(): ArrayCollection
    {
        return $this->likedPosts;
    }

    public function addLikedPosts(Post $post)
    {
        $this->likedPosts->add($post);
    }

    public function hasLikers(Post $post)
    {
        return $this->likedPosts->contains($post);
    }

    /**
     * @return mixed
     */
    public function getAmIFollower($id)
    {
        return $this->followers->contains($id);
    }


}
