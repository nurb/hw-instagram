<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Form\PostType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


/**
 * Note controller.
 *
 * @Route("post")
 */
class PostController extends Controller
{

    /**
     * @Route("/feed", name="feed")
     */
    public function indexAction()
    {
        $posts = $this->getDoctrine()
            ->getRepository('AppBundle:Post')
            ->findAll();

        $formButton = $this->createFormBuilder()
            ->setMethod('PUT')
            ->getForm();

        return $this->render('@App/Basic/feed.html.twig', array(
            'posts' => $posts,
            "form_button" => $formButton,
        ));
    }

    /**
     * @Route("/{id}/like", requirements={"id": "\d+"}, name="like_post")
     * @Method({"PUT"})
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function likePostAction(Request $request, int $id)
    {
        $post = $this->getDoctrine()
            ->getRepository('AppBundle:Post')
            ->find($id);
        if ($post->getisLiked($this->getUser())) {
            $post->removeLikers($this->getUser());
        } else {
            $post->addLikers($this->getUser());
        }
        $em = $this
            ->getDoctrine()
            ->getManager();
        $em->flush();
        return $this->redirect($request->headers->get('referer'));
    }
}
