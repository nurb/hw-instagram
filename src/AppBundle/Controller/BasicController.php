<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class BasicController extends Controller
{

    /**
     * @Route("/", name="homepage")
     */
    public function welcomeAction()
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('feed');
        }
        return $this->render('@App/Basic/index.html.twig', array());
    }
}
