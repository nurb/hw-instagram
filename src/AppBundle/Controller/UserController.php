<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Form\PostType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Note controller.
 *
 * @Route("user")
 */
class UserController extends Controller
{
    /**
     * @Route("/{id}/follow", requirements={"id": "\d+"}, name="follow_user")
     * @Method({"PUT"})
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function followUserAction(Request $request, int $id)
    {
        $targetUser = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($id);
        $loggedUser = $this->getUser();

        if ($targetUser->getAmIFollower($loggedUser)) {
            $loggedUser->removeFollows($targetUser);
        } else {
            $loggedUser->addFollows($targetUser);
        }

        $em = $this
            ->getDoctrine()
            ->getManager();
        $em->flush();
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("/my-profile", name="my_profile")
     * @Method({"GET","HEAD", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function myProfileAction(Request $request)
    {
        $posts = $this->getDoctrine()
            ->getRepository('AppBundle:Post')
            ->findAll();

        $newPost = new Post();
        $newPostForm = $this->createForm(PostType::class, $newPost, array(
            'method' => 'POST'
        ));
        $newPostForm->handleRequest($request);

        $formButton = $this->createFormBuilder()
            ->setMethod('PUT')
            ->getForm();

        if ($newPostForm->isSubmitted() && $newPostForm->isValid()) {
            $newPost->setUser($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($newPost);
            $em->flush();
            return $this->redirectToRoute("my_profile");
        }

        return $this->render('@App/User/my_profile.html.twig', array(
            'posts' => $posts,
            "form_button" => $formButton,
            "new_post_form" => $newPostForm
        ));
    }

}
